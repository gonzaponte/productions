#!/bin/bash

cd /gpfs0/arazi/users/gonzalom
source .bash_profile

echo Start date $(date)

folder={folder}
vfile={v_file}
outputfile={output_file}
nelectrons={n_electrons}
stepsize={step_size}
log={log_file}
err={err_file}

cd $folder
(time ./build/liquid $vfile $outputfile $nelectrons $stepsize) 1> >(tee $log) 2> >(tee $err >&2)

echo End date $(date)

echo "DONE"
