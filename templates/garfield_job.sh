##PBS options
#PBS -N {job_name}
#PBS -q {queue}
#PBS -l mem={memory}
#PBS -M gonzaponte@gmail.com
#PBS -m bae
#PBS -o {log_file}
#PBS -e {err_file}

cd
source .bash_profile

setup_garfield

echo Start date $(date)

exe={exe}
meshfile={mesh_file}
dielectricsfile={dielectrics_file}
vfile={v_file}
outputfile={output_file}
z0={z0}
nelectrons={n_electrons}
log={log_file}
err={err_file}

(time $exe $meshfile $dielectricsfile $vfile $outputfile $z0 $nelectrons) 1> >(tee $log) 2> >(tee $err >&2)

echo End date $(date)

echo "DONE"
