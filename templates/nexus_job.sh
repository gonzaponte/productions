##PBS options
#PBS -N {job_name}
#PBS -q {queue}
#PBS -l mem={memory}
#PBS -M gonzaponte@gmail.com
#PBS -m bae
#PBS -o {log_file}
#PBS -e {err_file}

cd $HOME
source $HOME/.bash_profile

setup_nexus

echoo HOSTNAME
echoo PBS_O_HOST
echoo PBS_SERVER
echoo PBS_O_QUEUE
echoo PBS_O_WORKDIR
echoo PBS_O_INITDIR
echoo PBS_O_ROOTDIR
echoo PBS_ARRAYID
echoo PBS_ENVIRONMENT
echoo PBS_GPUFILE
echoo PBS_JOBID
echoo PBS_JOBNAME
echoo PBS_NODEFILE
echoo PBS_QUEUE
echoo PBS_SERVER_HOME
echoo TMPDIR
echoo PBS_TMPDIR
echoo PBS_O_TMPDIR

echo Start date $(date)

exe={nexus_path}/nexus
init={ini_file}

time $exe -b -n {evt_per_file} $init

setup_ic_prod

python {this_path}/programs/compress_nexus.py {fileout}.h5 {fileout}
rm {fileout}.h5

echo End date $(date)

echo "DONE"
