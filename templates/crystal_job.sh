##PBS options
#PBS -N {job_name}
#PBS -q {queue}
#PBS -l mem={memory}
#PBS -M gonzaponte@gmail.com
#PBS -m bae
#PBS -o {log_file}
#PBS -e {err_file}

cd
source .bash_profile

echo Start date $(date)

cd {crystal_path}

(time nix develop -c just run -n {evt_per_file} -e {early} -l {late}) 1> >(tee $log) 2> >(tee $err >&2)

echo End date $(date)

echo "DONE"
