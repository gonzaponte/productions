##PBS options
#PBS -N {job_name}
#PBS -q {queue}
#PBS -l mem={memory}
#PBS -M gonzaponte@gmail.com
#PBS -m bae
#PBS -o {log_file}
#PBS -e {err_file}

cd $HOME
source $HOME/.bash_profile

{ic_setup}

export NUMEXPR_MAX_THREADS=1
export OMP_NUM_THREADS=1

cnf={cnf_file}

time city {program} $cnf

echo "DONE"
