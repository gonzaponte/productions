##PBS options
#PBS -N {job_name}
#PBS -q {queue}
#PBS -l mem={memory}
#PBS -M gonzaponte@gmail.com
#PBS -m bae
#PBS -o {log_file}
#PBS -e {err_file}

cd
source .bash_profile

setup_ic_prod

echoo HOSTNAME
echoo PBS_O_HOST
echoo PBS_SERVER
echoo PBS_O_QUEUE
echoo PBS_O_WORKDIR
echoo PBS_O_INITDIR
echoo PBS_O_ROOTDIR
echoo PBS_ARRAYID
echoo PBS_ENVIRONMENT
echoo PBS_GPUFILE
echoo PBS_JOBID
echoo PBS_JOBNAME
echoo PBS_NODEFILE
echoo PBS_QUEUE
echoo PBS_SERVER_HOME
echoo TMPDIR
echoo PBS_TMPDIR
echoo PBS_O_TMPDIR

cd {program_path}
cd ..
source setup.sh
cd -

exe=merge_h5s.py
fin={folderin}
fout={fileout}
log={log_file}
err={err_file}
options={options}
(time python $exe -i $fin -o $fout $options) 1> >(tee $log) 2> >(tee $err >&2)

echo "DONE"
