#!/usr/bin/python
from __future__ import print_function

import os

from external import argparse
from utils    import make_dirs
from utils    import write
from utils    import send_job
from utils    import print_cli
from utils    import replace_extension
from utils    import defaults


parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input-folder", type=os.path.abspath)
parser.add_argument("-o", "--output-file" , type=os.path.abspath, default = "merged.h5"    )
parser.add_argument("-q", "--queue"       , type=str            , default = defaults.queue )
parser.add_argument("-m", "--memory"      , type=str            , default = defaults.memory)
parser.add_argument("--ignore-empty"      , action="store_true" )
parser.add_argument("--dry-run"           , action="store_true" )
parser.add_argument("-v", "--verbose "    , action="count"      )

args = parser.parse_args()
print("="*50)
print_cli(args)
print("="*50)

folderin      = args. input_folder
fileout       = args.output_file
queue         = args.queue
memory        = args.memory
options       = "--ignore-empty" if args.ignore_empty else ""
basename      = os.path.basename(fileout)
output_folder = os.path. dirname(fileout)
program_path  = os.path.abspath(os.path.dirname(__file__))
program_path  = os.path.join(program_path, "programs")

job_template = open("templates/h5_merge_job.sh").read()

make_dirs(output_folder)

log_file = os.path.join(output_folder, replace_extension(basename, ".log"))
err_file = os.path.join(output_folder, replace_extension(basename, ".err"))
job_file = os.path.join(output_folder, replace_extension(basename, ".sh" ))
job_name = "h5merge"

print("Writing", job_file)
write(job_file, job_template, locals())

print("Queueing", job_file)
if not args.dry_run:
    send_job(job_file, 1000000)
