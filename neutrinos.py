release           = "v1_04_00"
nexus_release     = "v5_03_04"
IC_release        = "v0.9.10"
python_version    = "3.7"

path_to_home    = "/data5/users/gmartinez/topo/"
path_to_sw      = path_to_home    + "sw/"
path_to_conda   = path_to_sw      + "anaconda3/"
path_to_IC      = path_to_sw      + "IC_{IC_release}/"

path_to_scratch = path_to_home    + "data/"
path_to_exe     = path_to_scratch + "exe/"
path_to_job     = path_to_scratch + "job/"
path_to_cnf     = path_to_scratch + "cnf/"
path_to_data    = path_to_scratch + "data/"
path_to_logs    = path_to_scratch + "logs/"
path_to_log     = path_to_logs    + "log/"
path_to_err     = path_to_logs    + "err/"
path_to_out     = path_to_logs    + "out/"

sub_template = "templates/job_submission.sh"
sub_template = open(sub_template).read()
