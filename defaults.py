from os       import environ as env
from os       import path
from getpass  import getuser
from textwrap import dedent


class Neutrinos:
    name       = getuser()
    max_jobs   =     100
    queue      =   "long"
    memory     =    "2GB"
    delay      =     0.5
    nexus_path = path.join(env.get("SW", ""), "nexus", "bin")
    qsub       = "generic"
    ic_setup   = "setup_ic_prod"


class Next: # LSC
    name     = getuser()
    max_jobs =     200
    queue    =  "short"
    memory   =    "1GB"
    delay    =    1e-3
    qsub     = "generic"
    ic_setup = dedent("""
    source /data/software/miniconda/etc/profile.d/conda.sh
    export ICTDIR=/data/software/IC
    export ICDIR=$ICTDIR/invisible_cities
    export PATH="$ICTDIR/bin:$PATH"
    export PYTHONPATH=$ICTDIR:$PYTHONPATH
    conda activate IC-3.8-2022-04-13
    """
    )

class Majorana:
    name       = getuser()
    max_jobs   =      50
    queue      = "medium"
    memory     =    "1GB"
    delay      =     0.5
    nexus_path = path.join(env.get("SW", ""), "nexus", "build")
    qsub       = "generic"
    ic_setup   = "setup_ic_prod"


class Petalo:
    name       = getuser()
    max_jobs   =     100
    queue      = "medium"
    memory     =    "1GB"
    delay      =     0.5
    nexus_path = path.join(env.get("SW", ""), "nexus", "build")
    qsub       = "generic"
    ic_setup   = "setup_ic_prod"


class Shira:
    name       = getuser()
    max_jobs   = None
    queue      = None
    memory     = None
    delay      = 0.5
    nexus_path = path.join(env.get("GITDIR", ""), "nexus", "bin")
    qsub       = "bgu"
    ic_setup   = "setup_ic_prod"


class Bhn: # BGU
    name       = getuser()
    max_jobs   = 100
    queue      = "arazi.q"
    memory     = "2G"
    delay      = 0.5
    nexus_path = "/gpfs0/arazi/users/gonzalom/sw/nexus/build/"
    qsub       = "bgu"
    ic_setup   = "setup_ic_prod"
