import os
import sys
import argparse
import numpy  as np
import pandas as pd
import tables as tb

from invisible_cities.io  .dst_io            import df_writer
from invisible_cities.reco.paolina_functions import voxelize_hits
from invisible_cities.reco.paolina_functions import make_track_graphs
from invisible_cities.reco.paolina_functions import get_track_energy
from invisible_cities.evm .event_model       import BHit

sys.path.append(os.path.dirname(os.path.dirname(__file__)))

from utils import concat_output


def build_bhits(df):
    return [BHit(row.x, row.y, row.z, row.energy) for _, row in df.iterrows()]


def main_track_energy(df):
    hits   = build_bhits(df)
    voxels = voxelize_hits(hits, np.ones(3, dtype=float)*2)
    tracks = make_track_graphs(voxels)

    main_track = sorted(tracks, key=get_track_energy, reverse=True)[0]
    return get_track_energy(main_track)


def process_evt(df):
    particles = df.groupby("particle_id", as_index=False).sum().sort_values("energy").tail(2)
    onedf     = df.merge(particles.tail(1), how="inner", on="particle_id", suffixes = ("_hit", "_part"))
    twodf     = df.merge(particles.head(1), how="inner", on="particle_id", suffixes = ("_hit", "_part"))
    total_e   = df.energy.sum()
    long1_e   = onedf.energy_hit.sum()
    long2_e   = twodf.energy_hit.sum()
    total_n   = len(df)
    long1_n   = len(onedf)
    long2_n   = len(twodf)
    max_r     = (df.x**2 + df.y**2).max()**0.5
    min_z     = df.z.min()
    max_z     = df.z.max()
    ave_x     = onedf.x_hit.mean()
    ave_y     = onedf.y_hit.mean()
    ave_z     = onedf.z_hit.mean()
    event_id  = df.event_id.drop_duplicates().values[0]
    main_e    = main_track_energy(df)
    return pd.DataFrame(dict( event_id = [event_id]
                            , energy   = [total_e]
                            , energy1  = [long1_e]
                            , energy2  = [long2_e]
                            , nhits    = [total_n]
                            , nhits1   = [long1_n]
                            , nhits2   = [long2_n]
                            , max_r    = [max_r]
                            , min_z    = [min_z]
                            , max_z    = [max_z]
                            , ave_x    = [ave_x]
                            , ave_y    = [ave_y]
                            , ave_z    = [ave_z]
                            , main_e   = [main_e]
                            ))


def is_pp(df):
    return ("conv" in df.final_proc.values) and ("e+" in df.particle_name.values)


@concat_output
def summarize(filename):
    parts = ( pd.read_hdf(filename, "/MC/particles")
             .groupby("event_id", as_index=False)
#             .loc[lambda df: (df.final_proc == "conv")]
             .filter(is_pp)
             .event_id
             .drop_duplicates()
             .values
            )

    hits = ( pd.read_hdf(filename, "/MC/hits")
            .loc[lambda df: df.label=="ACTIVE"]
            .drop(columns="label")
            .groupby("event_id", as_index=False)
            .apply(process_evt)
           )
    return hits.assign(conv=hits.event_id.isin(parts))

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input-files-or-folders", type=os.path.abspath, nargs="*")
parser.add_argument("-o", "--output-file"           , type=os.path.abspath, default="pp_summary.h5")

args = parser.parse_args(sys.argv[1:])

for input_file_or_folder in args.input_files_or_folders:
    print(input_file_or_folder)
    df = summarize(input_file_or_folder)
    with tb.open_file(args.output_file, "w") as file:
        df_writer(file, df, "summary", "data")
