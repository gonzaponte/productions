import os
import sys
import glob

import numpy  as np
import pandas as pd
import tables as tb

from invisible_cities.io.dst_io import df_writer


def pixelize(filename, xybins):
    charge = \
    (pd.read_hdf(filename, "/MC/hits")
       .assign( xbin = lambda df: np.digitize(df.x, xybins)
              , ybin = lambda df: np.digitize(df.y, xybins)
              )
       .groupby("event_id xbin ybin".split())
       .energy
       .sum()
       .reset_index()
    )

    sensors = \
    (pd.read_hdf(filename, "/MC/sns_response")
       .loc[lambda df: df.sensor_id >= 1000]
       .groupby("event_id sensor_id".split())
       .charge
       .sum()
       .reset_index()
    )
    return charge, sensors

folder, outputfile = sys.argv[1:]
filenames          = sorted(glob.glob(os.path.join(folder, "*")))
xybins             = np.arange(-250, 251, 5)

bins = pd.DataFrame(dict( bin = np.digitize(xybins[:-1], xybins)
                        , low = xybins[:-1]
                        , upp = xybins[1:]
                        , mid = xybins[:-1] + np.diff(xybins)/2
                        )
                   )

pixelize_ = lambda f: pixelize(f, xybins)
charge, sensors = zip(*map(pixelize_, filenames))
charge  = pd.concat(charge , ignore_index=True)
sensors = pd.concat(sensors, ignore_index=True)
with tb.open_file(outputfile, "w") as file:
    df_writer(file, charge , "pixels", "charge")
    df_writer(file, sensors, "pixels", "sensors")
    df_writer(file, bins   , "pixels", "bins")
