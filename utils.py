from __future__ import print_function

import os
import time
import math
import subprocess as sp

from itertools import takewhile

import defaults as _defaults


class Seed:
    """
    Self-updating seed intended to be used in format-strings like
    >>> seed = Seed()
    >>> f"seed1 {seed}" # some seed
    >>> f"seed1 {seed}" # next seed

    Terrible in terms of robustness and reproducibility, but very
    comfortable to use.
    """
    def __init__(self, seed = "auto"):
        if seed == "auto":
            seed = self.seed_from_current_time()
        self.seed = seed

    def seed_from_current_time(self):
        current_time = time.time()
        seed         = (current_time % 10**6) * 100
        return int(seed)

    def __str__(self):
        s = str(self.seed)
        self.seed += 1
        return s

    def __repr__(self):
        return self.__str__()


def make_dir(path):
    """
    Create directories needed for path.
    Equivalent to running `mkdir -p path` on bash
    """
    tokens = os.path.abspath(path).split("/")
    for i in range(2, len(tokens) + 1):
        path = "/".join(tokens[:i])
        if os.path.exists(path): continue

        print("Creating", path)
        os.mkdir(path)


def make_dirs(*paths):
    """
    Map of `make_dir` over many paths
    """
    for path in paths:
        make_dir(path)


def join_dicts(*args):
    """
    Join dictionaries. The values of a dictionary override previous ones
    for the same key.
    """
    d = {}
    for a in args:
        d.update(a)
    return d


def decode_if_necessary(s):
    try   : return s.decode()
    except: return s


def write(filename, template, *variables):
    """
    Write a template file. Change mod if executable.
    """
    open(filename, "w").write(template.format(**join_dicts(*variables)))

    if filename.endswith(".sh"):
        os.system("chmod a+x {filename}".format(filename=filename))


def hostname():
    hostname = sp.Popen("hostname".split(), stdout=sp.PIPE).communicate()[0]
    hostname = decode_if_necessary(hostname)

    result = "".join(takewhile(str.isalpha, hostname.strip().split(".")[0]))
    if result == "frontend":
        result = "".join(takewhile(str.isalpha, hostname.strip().split("1")[1]))

    return result

def load_defaults():
    return getattr(_defaults, hostname().capitalize())


def jobs_in_queue():
    """
    Get the number of jobs in the queue.
    """
    command = "qstat -u {0}".format(defaults.name).split()
    process = sp.Popen(command, stdout=sp.PIPE, stderr=sp.PIPE)
    while process.poll() is None:
        time.sleep(.1)

    lines = list(filter(len, map(str.strip, map(decode_if_necessary, process.stdout.readlines()))))
    return lines


def njobs_in_queue():
    lines = jobs_ids()
    return len(jobs_ids())


def jobs_ids():
    lines  = jobs_in_queue()
    jobids = []
    for line in lines:
        if line and not line[0].isdigit(): continue

        jobids.append(line.split(" ")[0].split(".")[0])
    return jobids


def queue_is_full(nmax, verbose=True):
    """
    Check whether the queue is full.
    """
    try:
        nqueued = njobs_in_queue()
    except Exception as e:
        if verbose:
            print("jobs_in_queue raised an exception:")
            print(e)
            print("----------------------------------")
        return True

    return nqueued >= nmax


def wait_for_queue_to_empty(nmax, delay=5, verbose=True):
    """
    Check the availability of the queue until it is not full.
    """
    while queue_is_full(nmax, verbose=verbose):
        time.sleep(delay)


def run_qsub_generic(jobfile, max_retries=10, retry_delay=2, dry_run=False, **options):
    """
    Run the qsub command and retry if necessary.
    """
    command = "qsub {jobfile}".format(jobfile=jobfile).split()
    if dry_run:
        print("command: ", command)
        return

    for k in range(1, max_retries + 1):
        process = sp.Popen(command, stdout=sp.PIPE, stderr=sp.PIPE)
        while process.poll() is None:
            time.sleep(.1)

        output = ( decode_if_necessary(process.stdout.read())
                 + "\n"
                 + decode_if_necessary(process.stderr.read()))
        if "credential" not in output:
            print(output)
            break

        print("Failed {k} times.".format(k=k), end=" ")
        print("Retrying..." if k == max_retries else
              "Reached max number of attempts. Skipping.")

        time.sleep(retry_delay)


def run_qsub_bgu(jobfile, max_retries=10, retry_delay=2, dry_run=False, **options):
    """
    Run the qsub command and retry if necessary.
    """
    command = "qsub -q {queue} -l mem_free={memory} -o {log_file} -e {err_file} {jobfile}"
    command = command.format(jobfile=jobfile, **options).split()
    if dry_run:
        print("command: ", command)
        return

    for k in range(1, max_retries + 1):
        process = sp.Popen(command, stdout=sp.PIPE, stderr=sp.PIPE)
        while process.poll() is None:
            time.sleep(.1)

        output = ( decode_if_necessary(process.stdout.read())
                 + "\n"
                 + decode_if_necessary(process.stderr.read()))
        if "credential" not in output:
            print(output)
            break

        print("Failed {k} times.".format(k=k), end=" ")
        print("Retrying..." if k == max_retries else
              "Reached max number of attempts. Skipping.")

        time.sleep(retry_delay)


def send_job(jobfile, max_jobs, max_retries=10, wait_delay=5, retry_delay=2, dry_run=False, **qsub_options):
    """
    Wait for the queue to have room for more jobs and send a new one.
    """
    if not dry_run:
        wait_for_queue_to_empty(max_jobs, wait_delay)

    run_qsub = globals()["run_qsub_" + defaults.qsub]
    run_qsub(jobfile, max_retries, retry_delay, dry_run=dry_run, **qsub_options)


def get_indices(filename):
    """
    Get a multi-index for the file. Any field that is only contains digits
    is considered. Fields are separated by `.` or `_`.
    """
    filename = os.path.basename(filename)

    tokens  = (token for token_ in filename.split("_") for token in token_.split("."))
    indices = map(int, filter(str.isdigit, tokens))
    return tuple(indices)


def check_detector(detector):
    """
    Validate detector string.
    """
    if detector not in ("NEW", "NEXT100"):
        raise ValueError("Unrecognized detector: >{detector}<".format(detector=detector))


def get_n_events_per_job(total, n_files):
    """
    Get the number of events per job given the total number of events
    and the number of files it is split into.
    """
    return int(math.ceil(total * 1.0 / n_files))


def get_n_jobs(total, n_events_per_job):
    """
    Get the number of jobs given the total number of events
    and the number of events per job.
    """
    return int(math.ceil(total / n_events_per_job))


def print_cli(args):
    """
    Pretty-print the arguments from ArgumentParser.
    """
    for arg, value in args._get_kwargs():
        print("{arg} : {value}".format(arg=arg, value=value))


def combine_indices(*indices):
    """
    Combine indices into a string.
    """
    return "_".join(map(str, indices))


def replace_extension(path, new):
    if new.startswith("."):
        new = new[1:]
    return ".".join(path.split(".")[:-1] + [new])


def smart_int(s):
    return int(eval(s))


def concat_output(f):
    def g(filename_s):
        if isinstance(filename_s, (list, tuple)):
            dfs = tuple(map(f, filename_s))
            return pd.concat(dfs, ignore_index=True)
        elif os.path.isdir(filename_s):
            return g(glob.glob(os.path.join(filename_s, "*")))
        else:
            return f(filename_s)
    return g


def safe_remove(filename):
    if os.path.exists(filename):
        os.remove(filename)


def histogram(data, bins=100, low=None, upp=None):
    if isinstance(bins, int):
        low  = min(data) if low is None else low
        upp  = max(data) if upp is None else upp
        upp += 0.1 * (upp - low)
        bin_width = (upp - low) / bins
    elif isinstance(bins, (list, tuple)):
        low  = bins[0]
        upp  = bins[-1]
        bin_width = bins[1] - bins[0]
        bins = len(bins) - 1
    else:
        raise TypeError("Invalid type for bins: {0}".format(type(bins)))

    heights = [0] * bins
    binc    = [low + bin_width for i in range(bins+1)]
    for d in data:
        index = int((d - low) / bin_width)
        heights[index] += 1
    return heights, low, upp


defaults  = load_defaults()
prod_path = os.path.dirname(os.path.abspath(__file__))
