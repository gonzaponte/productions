#!/usr/bin/python
from __future__ import print_function

import os
import sys
import time
import subprocess as sp

from multiprocessing import Pool

from external import argparse

def run_job(job_file):
    t0 = time.time()
    for line in open(job_file):
        if line.startswith("#PBS -o"):
            log_filename = line.split(" ")[2].strip()
        if line.startswith("#PBS -e"):
            err_filename = line.split(" ")[2].strip()

    print("Running", job_file)
    with open(log_filename, "w") as log_file:
        with open(err_filename, "w") as err_file:
            sp.Popen(["bash", job_file], stdout=log_file, stderr=err_file).wait()

    dt = time.time() - t0
    print("Finished {job_file} in {dt:.1f} seconds".format(**locals()))

parser = argparse.ArgumentParser()
parser.add_argument("jobs", type=os.path.abspath, nargs="+")
parser.add_argument("-n", "--nproc", type=int, default=2)

args = parser.parse_args(sys.argv[1:])

p = Pool(args.nproc)
p.map(run_job, sorted(args.jobs), chunksize=1)
